﻿namespace test2fa
{
    using System;
    using System.Text;
    using System.Security.Cryptography;

    class CodeGenerator
        {
            private int intervalLength;
            private int pinCodeLength;
            private int pinModulo;

            private byte[] Key = new byte[10]; // generate Bytearray out of String (80 Bit)
            public string enteredKey;

            public CodeGenerator(string enteredKey)
            {
                pinCodeLength = 6;
                intervalLength = 30;
                pinModulo = (int)Math.Pow(10, pinCodeLength);

                Key = Encoding.UTF8.GetBytes(enteredKey);
            }


            private String generateResponseCode(long challenge, byte[] secret)
            {
                HMACSHA1 myHmac = new HMACSHA1(secret);
                myHmac.Initialize();

                byte[] value = BitConverter.GetBytes(challenge);
                Array.Reverse(value);
                myHmac.ComputeHash(value);
                byte[] hash = myHmac.Hash;
                int offset = hash[hash.Length - 1] & 0xF;
                byte[] SelectedFourBytes = new byte[4];

                SelectedFourBytes[0] = hash[offset];
                SelectedFourBytes[1] = hash[offset + 1];
                SelectedFourBytes[2] = hash[offset + 2];
                SelectedFourBytes[3] = hash[offset + 3];
                Array.Reverse(SelectedFourBytes);
                int finalInt = BitConverter.ToInt32(SelectedFourBytes, 0);
                int truncatedHash = finalInt & 0x7FFFFFFF;
                int pinValue = truncatedHash % pinModulo;
                return padOutput(pinValue);
            }

            public long getCurrentInterval()
            {
                TimeSpan TS = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                long currentTimeSeconds = (long)Math.Floor(TS.TotalSeconds);
                long currentInterval = currentTimeSeconds / intervalLength; // 30 Seconds
                return currentInterval;
            }

            private String padOutput(int value)
            {
                String result = value.ToString();
                for (int i = result.Length; i < pinCodeLength; i++)
                {
                    result = "0" + result;
                }
                return result;
            }


            public string GeneratePin()
            {
                return generateResponseCode(getCurrentInterval(), Key);
            }
        }
}
