﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace test2fa
{
    public class Person
    {
        private string Json { get; set; }
        public void SerializeToJson(Dictionary<string, string> secretUser)
        {
            Json = JsonConvert.SerializeObject(secretUser);
            WriteToFile(Json);

        }

        private void WriteToFile(string json)
        {
            var sw = new StreamWriter(@"C:\tmp\json.txt");
            sw.Write(json);
            sw.Flush();
            sw.Close();
        }
    }
}
