﻿using System;
using System.Collections.Generic;
using System.Text;

namespace test2fa
{
    class CalculateTimer
    {
        public CalculateTimer()
        {
            
        }

        public long TimeLeftInEpoch()
        {
            TimeSpan TS = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            long currentTimeSeconds = (long)Math.Floor(TS.TotalSeconds);
            long currentTimer = 30 - (currentTimeSeconds % 30); // 30 Seconds
            return currentTimer;
        }
    }
}
