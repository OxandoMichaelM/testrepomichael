﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using test2fa.Annotations;
using Xamarin.Forms;

namespace test2fa
{
    public class MainPageModel :INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
      
        public ICommand SecretKeyCommand { get; set; }
      

        public string SecretKeyResult { get; set; }
        public string Username  { get; set; }
        public string TextSecret => SecretText;
        private Person _person;
        private const  string SecretText = "Please enter...";

        public MainPageModel()
        {
            _person = new Person();
            //SecretKeyCommand = new Command<Dictionary<string, string>>(CreateSecretKey);

            SecretKeyCommand = new Command<string>(CreateSecretKey);
        }
        


        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }



       


        //private void CreateSecretKey(Dictionary<string, string> entrySecretUsername)
        //{
           
        //   GenerateKey(entrySecretUsername.Keys.First(), entrySecretUsername.Values.First());
        //}

        private void CreateSecretKey(string entrySecretUsername)
        {
            GenerateKey(Username, entrySecretUsername);
            //GenerateKey(entrySecretUsername, entrySecretUsername.Values.First());
        }
        private void GenerateKey(string entryUsername, string entrySecret)
        {
            var totp = new CodeGenerator(entrySecret);
            var totpTimerCalculation = new CalculateTimer();

            Device.StartTimer(TimeSpan.FromSeconds(1), () => {
                // Device.BeginInvokeOnMainThread(() => totpTimerCalculation.TimeLeftInEpoch());
                Device.BeginInvokeOnMainThread(() => totp.GeneratePin());
                SecretKeyResult = totp.GeneratePin();
                _person.SerializeToJson(new Dictionary<string, string>{{entryUsername,entrySecret}});
                OnPropertyChanged(nameof(SecretKeyResult));
                //totpCode.Text = totp.GeneratePin();
                //totpTime.Text = totpTimerCalculation.TimeLeftInEpoch().ToString();
                return true;
            });
        }

    }
}
